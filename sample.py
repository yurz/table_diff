# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from table_diff import diff

# <codecell>

file_left = "sample/data_export_last_month.csv"
file_right = "sample/data_export_this_month.csv"

# <codecell>

key_fields = ["item","store location"]

# <codecell>

diff(file_left, file_right, key_fields)

# <codecell>


