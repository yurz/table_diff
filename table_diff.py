# -*- coding: utf-8 -*-
# 
# The MIT License (MIT)
# Copyright (C) 2014 Yuri Zhylyuk <yuri@zhylyuk.com>
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


def diff(file_left, file_right, key_fields, sqlite_path=None, fields_to_check=None, fields_to_ignore=None, keep_tables=False, diff_csv="diff.csv"):
    '''
    to do...
    '''

    import sqlite3
    from pandas import read_csv, read_sql

    def to_sqlite(sqlite_con, file_path, side=""):
        if side not in ["left", "right"]:
            print("you have to provide side - left or right")
            return 0
        df = read_csv(file_path, index_col=None, na_values='')
        df.to_sql(side, con=sqlite_con, if_exists='replace', index=None)

    if sqlite_path is None:
        con = sqlite3.connect(":memory:")
    else:
        try:
            con = sqlite3.connect(sqlite_path)
        except Exception as e:
            print("problem connecting to", sqlite_con, e)
    
    diff_tb = "diff"

    to_sqlite(con, file_left, side="left")
    to_sqlite(con, file_right, side="right")
    
    con.execute("drop table if exists diff_left")
    sql = "create table diff_left as select left.*, ([" + "] || '~' || [".join(key_fields) + "]) \
    as diff_id from left"
    con.execute(sql)
    con.commit()

    con.execute("drop table if exists diff_right")
    sql = "create table diff_right as select right.*, ([" + "] || '~' || [".join(key_fields) + "]) \
    as diff_id from right"
    con.execute(sql)
    con.commit()
    
    con.execute("create index diff_left_ix_01 on diff_left(diff_id)")
    con.execute("create index diff_right_ix_01 on diff_right(diff_id)")
    con.commit()

    if fields_to_check == None:
        result = con.execute("PRAGMA table_info(left)").fetchall()
        col_names = list(map(lambda x: x[1], result))
        exclude = key_fields.copy()
        exclude.append("diff_id")
        fields_to_check = [x for x in col_names if x not in exclude]
        
    if fields_to_ignore:
        fields_to_check = [x for x in fields_to_check if x not in fields_to_ignore]

    fields_to_check = ["[" + x + "]" for x in fields_to_check]
  
    cr_audit_tb = " text, ".join(fields_to_check)
    in_audit_tb = ", ".join(fields_to_check)
    
    con.execute("drop table if exists " + diff_tb)
    sql = "create table " + diff_tb + "(diff_id text, new_or_missing text, " + cr_audit_tb + ")"
    con.execute(sql)
    con.commit()

    sql1_draft = """ update """ + diff_tb + """
                     set &&field = (
                                     select ifnull(diff_left.&&field,'') || '  -->  ' || ifnull(diff_right.&&field,'') 
                                     from diff_left
                                     inner join diff_right on diff_right.diff_id = diff_left.diff_id 
                                          and ifnull(diff_right.&&field,'') <> ifnull(diff_left.&&field,'')
                                     where diff_left.diff_id = """ + diff_tb + """.diff_id
                                    )"""
    
    sql2_draft = """ insert into """ + diff_tb + """ (diff_id, &&field) 
                     select distinct diff_left.diff_id
                            ,ifnull(diff_left.&&field,'') || '  -->  ' || ifnull(diff_right.&&field,'') 
                     from diff_left
                     inner join diff_right on diff_right.diff_id = diff_left.diff_id 
                     where ifnull(diff_right.&&field, '') <> ifnull(diff_left.&&field,'')
                        and not exists (
                                         select null
                                         from """ + diff_tb + """
                                         where diff_id = diff_left.diff_id
                                        )"""
    
    for field in fields_to_check:
        sql = sql1_draft.replace("&&field", field)
        con.execute(sql)
        sql = sql2_draft.replace("&&field", field)
        con.execute(sql)
    con.commit()

    sql = "insert into " + diff_tb + " (diff_id, new_or_missing, "  + in_audit_tb + ") \
    select distinct diff_id, 'missing', " + in_audit_tb + " \
    from diff_left where diff_id not in (select ifnull(diff_id, '') from diff_right)"
    con.execute(sql)
    con.commit()

    sql = "insert into " + diff_tb + " (diff_id, new_or_missing, "  + in_audit_tb + ") \
    select distinct diff_id, 'new', " + in_audit_tb + " \
    from diff_right where diff_id not in (select ifnull(diff_id, '') from diff_left)"
    con.execute(sql)
    con.commit()

    if keep_tables != "yes":
        con.execute("drop table if exists diff_left")
        con.execute("drop table if exists diff_right")
        con.commit()
    
    all_chng = con.execute("select count(*) from " + diff_tb).fetchone()[0]
    if all_chng == 0:
        con.execute("drop table if exists " + diff_tb)
        con.commit()
        con.close()
        return 0, 0, 0, # missing, new, updated
    
    else:
        missing = con.execute("""select count(*) from """ + diff_tb + \
                              """ where new_or_missing = 'missing'""").fetchone()[0]
        new = con.execute("""select count(*) from """ + diff_tb + \
                              """ where new_or_missing = 'new'""").fetchone()[0]
        updated = all_chng - (missing + new)
        df = read_sql("select * from " + diff_tb, con)
        df.to_csv(diff_csv, index=None)
        con.close()
        return missing, new, updated